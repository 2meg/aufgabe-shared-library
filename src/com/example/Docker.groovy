#!/usr/bin/env groovy

package com.example

class Docker implements Serializable {

    def script

    Docker(script){
        this.script = script
    }

    def buildDockerImage(String imageTag, String path) {
        script.echo "Building golang docker image..."
            script.sh "docker build -t $imageTag $path"
    }


    def loginDocker(String repoLink) {
        script.withCredentials([script.usernamePassword(credentialsId: 'docker', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
            script.sh "echo $script.PWD | docker login -u $script.USER --password-stdin $repoLink"
        }
    }

    def pushNpmVersion(String repoLink, String branch_name, String versionPath) {
        script.withCredentials([script.usernamePassword(credentialsId: 'git_creds', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
            script.sh "git remote set-url origin https://$script.USER:$script.PWD@$repoLink"
            script.sh "git add $versionPath"
            script.sh "git commit -m 'jenkins version update'"
            script.sh "git push origin HEAD:$branch_name"
        }
    }

    def pushDockerImage(String imageTag) {
        script.sh "docker push $imageTag"
    }

    def commitID() {
        script.sh 'git rev-parse HEAD > .git/commitID'
        def commitID = script.readFile('.git/commitID').trim()
        script.sh 'rm .git/commitID'
        commitID
    }

}
