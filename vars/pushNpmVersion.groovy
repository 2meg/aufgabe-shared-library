#!/usr/bin/env groovy

import com.example.Docker

def call(String repoLink, String branch_name, String versionPath) {
    return new Docker(this).pushNpmVersion(repoLink, branch_name, versionPath)
}   