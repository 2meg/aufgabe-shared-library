#!/usr/bin/env groovy

import com.example.Docker

def call(String imageTag, String path) {
    return new Docker(this).buildDockerImage(imageTag, path)
}