#!/usr/bin/env groovy

import com.example.Docker

def call(String repoLink) {
    return new Docker(this).loginDocker(repoLink)
}