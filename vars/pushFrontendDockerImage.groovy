#!/usr/bin/env groovy

import com.example.Docker

def call(String imageTagFrontend) {
    return new Docker(this).pushFrontendDockerImage(imageTagFrontend)
}