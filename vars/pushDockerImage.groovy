#!/usr/bin/env groovy

import com.example.Docker

def call(String imageTag) {
    return new Docker(this).pushDockerImage(imageTag)
}